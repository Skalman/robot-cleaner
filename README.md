# Robot Cleaner

The Robot Cleaner can make 10,000 moves, taking up to 100,000 steps each time. The
total number of possible steps is thus 10,000 \* 100,000 = 1,000,000,000.

After having cleaned the room, it reports how many unique locations it has
cleaned. See the [instructions](./Documentation/Instructions.pdf) for further
details.

## Notes on performance

A naive solution could simply store all steps in a hash set. The performance would
be O(N\*M) for N moves and M steps, the memory usage O(N\*M).

My solution's performance is O(N^2) for N moves, and memory usage O(N).
This is slightly better performance, and much better memory usage.
