﻿using RobotCleaner.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RobotCleaner
{
    internal class Program
    {
        private static void Main()
        {
            var instructionCount = int.Parse(Console.ReadLine()!);

            // The initial position isn't needed.
            _ = Console.ReadLine();

            var instructions = ReadLines()
                .Take(instructionCount)
                .Select(Instruction.Parse);

            var robot = new RobotCleaner();
            var result = robot.RunInstructions(instructions);

            Console.WriteLine($"=> Cleaned: {result}");
        }

        private static IEnumerable<string> ReadLines()
        {
            while (true)
            {
                var line = Console.ReadLine();
                if (line == null)
                    break;

                yield return line;
            }
        }
    }
}
