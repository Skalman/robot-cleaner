﻿namespace RobotCleaner.Extensions
{
    public static class ValueTupleExtensions
    {
        /// <summary>
        /// Sorts the values of the tuple.
        /// </summary>
        public static (int Min, int Max) MinMax(this (int, int) x)
        {
            return x.Item1 < x.Item2
                ? (x.Item1, x.Item2)
                : (x.Item2, x.Item1);
        }
    }
}
