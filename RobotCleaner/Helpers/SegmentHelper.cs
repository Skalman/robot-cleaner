﻿using RobotCleaner.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RobotCleaner.Helpers
{
    public class SegmentHelper
    {
        /// <summary>
        /// Given a number of (one-dimensional) line segments, combine them to
        /// avoid any overlaps.
        /// </summary>
        public static List<(int Min, int Max)> GetNonOverlappingSegments(IEnumerable<(int Start, int End)> segments)
        {
            var orderedSegments = segments
                .Select(x => x.MinMax())
                .OrderBy(x => x.Min);

            var result = new List<(int Min, int Max)>();

            foreach (var (min, max) in orderedSegments)
            {
                // This inner while loop should be O(1) on average.
                var newMin = min;
                var newMax = max;
                while (result.Count != 0)
                {
                    var (lastMin, lastMax) = result.Last();

                    // Can we combine the current segment with the last one?
                    if (lastMax < min)
                        break;

                    newMin = Math.Min(newMin, lastMin);
                    newMax = Math.Max(newMax, lastMax);
                    result.RemoveAt(result.Count - 1);
                }

                result.Add((newMin, newMax));
            }

            return result;
        }
    }
}
