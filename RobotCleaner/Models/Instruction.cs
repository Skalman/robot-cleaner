﻿using System.Collections.Generic;
using System.Linq;

namespace RobotCleaner.Models
{
    public class Instruction
    {
        private static readonly Dictionary<string, Vector> Directions = new Dictionary<string, Vector>
        {
            { "E", Vector.East },
            { "W", Vector.West },
            { "N", Vector.North },
            { "S", Vector.South },
        };

        public Instruction(string direction, int length)
        {
            Direction = direction;
            Length = length;
        }

        public string Direction { get; }
        public int Length { get; }

        public static Instruction Parse(string str)
        {
            var parts = str.Split(" ");
            return new Instruction(parts[0], int.Parse(parts[1]));
        }

        public static IEnumerable<Instruction> Parse(IEnumerable<string> strings)
            => strings.Select(Parse);

        public Point GetNewPosition(Point originalPosition)
        {
            var vector = Directions[Direction] * Length;
            return originalPosition + vector;
        }
    }
}
