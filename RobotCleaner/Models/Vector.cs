﻿namespace RobotCleaner.Models
{
    public struct Vector
    {
        // Use a coordinate system where the Y axis increases toward the north.
        public static readonly Vector East = new Vector { X = 1 };
        public static readonly Vector West = new Vector { X = -1 };
        public static readonly Vector North = new Vector { Y = 1 };
        public static readonly Vector South = new Vector { Y = -1 };

        public int X { get; set; }

        public int Y { get; set; }

        public static Vector operator *(Vector v, int factor)
        {
            v.X *= factor;
            v.Y *= factor;
            return v;
        }
    }
}
