﻿using RobotCleaner.Enums;
using System;

namespace RobotCleaner.Models
{
    /// <summary>
    /// All lines are assumed to be either horizontal or vertical.
    /// </summary>
    public struct Line
    {
        public Line(Point p1, Point p2)
        {
            P1 = p1;
            P2 = p2;
        }

        public Point P1 { get; set; }

        public Point P2 { get; set; }

        public Orientation Orientation => P1.X == P2.X
            ? Orientation.Vertical
            : Orientation.Horizontal;

        public int Length => 1 + Math.Abs(
            P1.X == P2.X
                ? P1.Y - P2.Y
                : P1.X - P2.X);
    }
}
