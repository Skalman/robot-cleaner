﻿namespace RobotCleaner.Models
{
    public struct Point
    {
        // Avoid using properties here. These fields are part of performance
        // sensitive calculations.
        public int X;
        public int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Point operator +(Point p, Vector v)
        {
            return new Point
            {
                X = p.X + v.X,
                Y = p.Y + v.Y,
            };
        }
    }
}
