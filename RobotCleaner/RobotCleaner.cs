﻿using RobotCleaner.Enums;
using RobotCleaner.Helpers;
using RobotCleaner.Models;
using System.Collections.Generic;
using System.Linq;

namespace RobotCleaner
{
    public class RobotCleaner
    {
        /// <summary>
        /// Run the given instructions and return the number of unique
        /// locations that were cleaned.
        /// </summary>
        public int RunInstructions(IEnumerable<Instruction> instructions)
        {
            // Set the initial position to (0, 0). We only need to keep track
            // of relative positions.
            var curPos = default(Point);
            var lines = new List<Line>();
            foreach (var instruction in instructions)
            {
                var oldPos = curPos;
                curPos = instruction.GetNewPosition(oldPos);
                lines.Add(new Line(oldPos, curPos));
            }

            return GetUniqueNumberOfPoints(lines);
        }

        private static IEnumerable<Line> GetUniqueVerticalLines(IEnumerable<Line> lines)
        {
            return lines
                .GroupBy(line => line.P1.X)
                .SelectMany(group =>
                {
                    var x = group.Key;
                    var segments = SegmentHelper.GetNonOverlappingSegments(group.Select(line => (line.P1.Y, line.P2.Y)));
                    return segments.Select(segment => new Line(
                        new Point(x, segment.Min),
                        new Point(x, segment.Max)
                    ));
                });
        }

        private static IEnumerable<Line> GetUniqueHorizontalLines(IEnumerable<Line> lines)
        {
            return lines
                .GroupBy(line => line.P1.Y)
                .SelectMany(group =>
                {
                    var y = group.Key;
                    var segments = SegmentHelper.GetNonOverlappingSegments(group.Select(line => (line.P1.X, line.P2.X)));
                    return segments.Select(segment => new Line(
                        new Point(segment.Min, y),
                        new Point(segment.Max, y)
                    ));
                });
        }

        private static int GetUniqueNumberOfPoints(IEnumerable<Line> lines)
        {
            var lineLookup = lines.ToLookup(x => x.Orientation);
            var verticalLines = GetUniqueVerticalLines(lineLookup[Orientation.Vertical]).ToList();
            var horizontalLines = GetUniqueHorizontalLines(lineLookup[Orientation.Horizontal]).ToList();

            var verticalSum = verticalLines.Sum(x => x.Length);
            var horizontalSum = horizontalLines.Sum(x => x.Length);

            // These are the hot loops that should be optimized.
            // Performance: O(N^2)
            var intersectionCount = verticalLines
                .AsParallel()
                .Sum(v => {
                    var vy1 = v.P1.Y;
                    var vy2 = v.P2.Y;
                    var vx = v.P1.X;
                    var count = 0;
                    foreach (var h in horizontalLines)
                    {
                        var hx1 = h.P1.X;
                        var hx2 = h.P2.X;
                        var hy = h.P1.Y;
                        if (
                            // Vertical line's X is within the horizontal line.
                            hx1 <= vx && vx <= hx2

                            // Horizontal line's Y is within the vertical line.
                            && vy1 <= hy && hy <= vy2)
                        {
                            count++;
                        }
                    }
                    return count;
                });

            var result = verticalSum + horizontalSum - intersectionCount;

            return result;
        }
    }
}
