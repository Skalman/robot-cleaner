﻿namespace RobotCleaner.Enums
{
    public enum Orientation
    {
        Vertical,
        Horizontal,
    }
}
