-- Performance on my system:
-- Snake:           25 seconds
-- Square spirals:  30 seconds

declare
	@Before datetime,
	@Result int,
	@InstructionsString nvarchar(max)

-- Snake
set @InstructionsString = ''
;with X as (
	select N
	from (values (0), (1), (2), (3), (4), (5), (6), (7), (8), (9)) V(N)
),
Numbers as (
	select A.N + B.N*10 + C.N*100 + D.N*1000 N
	from X A, X B, X C, X D
)
select
	@InstructionsString += 
'
W 100000
S 1
E 100000
S 1
'
from Numbers
where N < 2500

set @Before = getdate()
set @Result = dbo.RunInstructions(
'
10000
0 0
'
+ @InstructionsString)

select
	'Snake' Test,
	@Result Result,
	500005001 ExpectedResult,
	datediff(ms, @Before, getdate()) DurationInMilliseconds,
	case when @Result = 500005001
		then 'OK'
		else 'ERROR'
	end Verdict


-- Square spirals
set @InstructionsString = ''
;with X as (
	select N
	from (values (0), (1), (2), (3), (4), (5), (6), (7), (8), (9)) V(N)
),
Numbers as (
	select A.N + B.N*10 + C.N*100 + D.N*1000 N
	from X A, X B, X C, X D
)
select
	@InstructionsString += 
'
E 100000
N 100000
W 99999
S 99999
'
from Numbers
where N < 2500

set @Before = getdate()
set @Result = dbo.RunInstructions(
'
10000
0 0
'
+ @InstructionsString)

select
	'Square spirals' Test,
	@Result Result,
	993747501 ExpectedResult,
	datediff(ms, @Before, getdate()) DurationInMilliseconds,
	case when @Result = 993747501
		then 'OK'
		else 'ERROR'
	end Verdict
