create or alter function Split
(
    @string nvarchar(max),
    @delimiter nvarchar(10)
)
returns @table table
(
	I int,
	Value nvarchar(4000)
)
begin
	if @string is null or @delimiter is null
		return

	-- http://stackoverflow.com/a/314833

    declare
		@nextString nvarchar(4000),
		@pos int,
		@nextPos int,
		@rownum int = 0

    set @string += @delimiter

    set @pos = charindex(@delimiter, @string)
    set @nextPos = 1
    while @pos <> 0
    begin
        set @nextString = substring(@string, 1, @pos - 1)

        insert into @table (I, Value)
        values (@rownum, @nextString)

        set @string = substring(@string, @pos + datalength(@delimiter)/2, datalength(@string)/2)
        set @nextPos = @pos
        set @pos = charindex(@delimiter, @string)
		set @rownum += 1
    end
    return
end
