create or alter procedure RobotCleaner
	@InstructionsString nvarchar(max)
as
begin
	print concat('=> ', dbo.RunInstructions(@InstructionsString))
end
go
exec RobotCleaner '
2
10 22
E 2
N 1
'
