with TestCases as (
select
'Example' TestCase,
'
2
10 22
E 2
N 1
' InstructionsString,
4 ExpectedResult

union all select
'Single move',
'
1
0 0
W 12
' InstructionsString,
13 ExpectedResult

union all select
'Back and forth, east <-> west',
'
2
0 0
E 2
W 6
' InstructionsString,
7 ExpectedResult

union all select
'Back and forth, south <-> north',
'
2
0 0
S 55
N 10
' InstructionsString,
56 ExpectedResult

union all select
'Back and forth, intersection',
'
5
0 0
E 2
W 6
S 10
E 5
N 10
' InstructionsString,
1+6+10+5+10-1 ExpectedResult

-- Continue
union all select
'Continue',
'
5
0 0
N 10
N 10
E 5
S 10
W 20
' InstructionsString,
1+10+10+5+10+20-1 ExpectedResult

-- Square
union all select
'Square',
'
5
0 0
N 5
N 5
E 10
S 10
W 10
' InstructionsString,
4*10 ExpectedResult
),
TestCasesWithResults as (
	select
		*,
		dbo.RunInstructions(InstructionsString) Result
	from TestCases
)
select
	*,
	case when ExpectedResult = Result
		then 'OK'
		else 'ERROR'
	end Verdict
from TestCasesWithResults
