create or alter function RunInstructions
(
	@InstructionsString nvarchar(max)
)
returns int
as
begin
	-- Replace \r\n with \n
	set @InstructionsString = replace(@InstructionsString, char(13) + char(10), char(10))

	-- Parse instructions to vectors and convert them into lines.
	begin
		declare @Lines table
		(
			Rank int primary key,
			VectorX int,
			VectorY int,
			Point1X int,
			Point1Y int,
			Point2X int,
			Point2Y int,
			Orientation nvarchar(3)
		)

		;with InputLines as (
			select
				Value,
				rank() over (order by I) Rank
			from dbo.Split(@InstructionsString, char(10))
			where Value != ''
		),
		Instructions as (
			select
				InputLines.Rank,
				max(case when InstructionPart.I = 0 then InstructionPart.Value end) Direction,
				cast(max(case when InstructionPart.I = 1 then InstructionPart.Value end) as int) Length
			from InputLines
				cross apply dbo.Split(InputLines.Value, ' ') InstructionPart
			where InputLines.Rank >= 3 -- 1=instruction count, 2=initial position
			group by InputLines.Rank
		),
		Directions as (
			select 'E' Direction, 1 X, 0 Y
			union all select 'W', -1, 0
			union all select 'N', 0, 1
			union all select 'S', 0, -1
		),
		Vectors as (
			select
				Instructions.Rank,
				Instructions.Length * Directions.X X,
				Instructions.Length * Directions.Y Y
			from Instructions
			join Directions
				on Instructions.Direction = Directions.Direction
		)
		insert into @Lines (Rank, VectorX, VectorY, Orientation)
		select
			Rank,
			X,
			Y,
			case when X = 0
				then 'Ver'
				else 'Hor'
			end
		from Vectors
	
		declare
			@curPosX int = 0,
			@curPosY int = 0

		update @Lines
		set
			@curPosX += VectorX,
			@curPosY += VectorY,
			Point1X = @curPosX - VectorX,
			Point1Y = @curPosY - VectorY,
			Point2X = @curPosX,
			Point2Y = @curPosY
	end

	declare @Rank int, @Min int, @Max int

	-- Handle horizontal lines
	begin
		declare @HorizontalLines table
		(
			Rank int primary key,
			X1 int,
			X2 int,
			Y int,
			WillCombine bit
		)

		insert into @HorizontalLines (Rank, X1, X2, Y, WillCombine)
		select
			Rank,
			case when Point1X < Point2X then Point1X else Point2X end,
			case when Point1X < Point2X then Point2X else Point1X end,
			Point1Y,
			0
		from @Lines
		where Orientation = 'Hor'

		-- Performance: This loop is not efficient
		set @Rank = 3
		while @Rank is not null
		begin
			update Line
			set WillCombine = 1
			from @HorizontalLines CurLine
				join @HorizontalLines Line
					on CurLine.Y = Line.Y
					and CurLine.X1 <= Line.X2
					and Line.X1 <= CurLine.X2
					and CurLine.Rank != Line.Rank
			where CurLine.Rank = @Rank

			select
				@Min = min(X1),
				@Max = max(X2)
			from @HorizontalLines
			where WillCombine = 1

			if @Min is not null
			begin
				delete from @HorizontalLines
				where WillCombine = 1

				update @HorizontalLines
				set
					X1 = case when @Min < X1 then @Min else X1 end,
					X2 = case when @Max > X2 then @Max else X2 end
				where Rank = @Rank
			end

			select @Rank = min(Rank)
			from @HorizontalLines
			where Rank > @Rank
		end
	end

	-- Handle vertical lines
	begin
		declare @VerticalLines table
		(
			Rank int primary key,
			Y1 int,
			Y2 int,
			X int,
			WillCombine bit
		)

		insert into @VerticalLines (Rank, Y1, Y2, X, WillCombine)
		select
			Rank,
			case when Point1Y < Point2Y then Point1Y else Point2Y end,
			case when Point1Y < Point2Y then Point2Y else Point1Y end,
			Point1X,
			0
		from @Lines
		where Orientation = 'Ver'

		-- Performance: This loop is not efficient
		set @Rank = 3
		while @Rank is not null
		begin
			update Line
			set WillCombine = 1
			from @VerticalLines CurLine
				join @VerticalLines Line
					on CurLine.X = Line.X
					and CurLine.Y1 <= Line.Y2
					and Line.Y1 <= CurLine.Y2
					and CurLine.Rank != Line.Rank
			where CurLine.Rank = @Rank

			select
				@Min = min(Y1),
				@Max = max(Y2)
			from @VerticalLines
			where WillCombine = 1

			if @Min is not null
			begin
				delete from @VerticalLines
				where WillCombine = 1

				update @VerticalLines
				set
					Y1 = case when @Min < Y1 then @Min else Y1 end,
					Y2 = case when @Max > Y2 then @Max else Y2 end
				where Rank = @Rank
			end
		
			select @Rank = min(Rank)
			from @VerticalLines
			where Rank > @Rank
		end
	end

	declare
		@HorizontalSum int,
		@VerticalSum int,
		@IntersectionCount int

	select @HorizontalSum = isnull(sum(1 + X2 - X1), 0)
	from @HorizontalLines

	select @VerticalSum = isnull(sum(1 + Y2 - Y1), 0)
	from @VerticalLines

	-- Performance: This is necessarily slow. But SQL Server optimizes it well.
	select @IntersectionCount = count(*)
	from @HorizontalLines H
		join @VerticalLines V
			on H.Y between V.Y1 and V.Y2
			and V.X between H.X1 and H.X2


	return @HorizontalSum + @VerticalSum - @IntersectionCount
end
go


select dbo.RunInstructions('
2
10 22
E 2
N 1
') Result
