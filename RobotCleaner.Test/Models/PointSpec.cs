using RobotCleaner.Models;
using Shouldly;
using Xunit;

namespace RobotCleaner.Test.Models
{
    public class PointSpec
    {
        private Point P(int x, int y) => new Point(x, y);

        [Fact]
        public void OperatorPlus()
        {
            (P(5, 5) + Vector.East * 5)
                .ShouldBe(P(10, 5), "East");

            (P(5, 5) + Vector.West * 5)
                .ShouldBe(P(0, 5), "West");

            (P(5, 5) + Vector.North * 5)
                .ShouldBe(P(5, 10), "North");

            (P(5, 5) + Vector.South * 5)
                .ShouldBe(P(5, 0), "South");
        }
    }
}
