using RobotCleaner.Models;
using Shouldly;
using Xunit;

namespace RobotCleaner.Test.Models
{
    public class VectorSpec
    {
        [Fact]
        public void VectorEquals()
        {
            (Vector.East * 5).ShouldBe(new Vector { X = 5 }, "East");
            (Vector.West * 5).ShouldBe(new Vector { X = -5 }, "West");
            (Vector.North * 5).ShouldBe(new Vector { Y = 5 }, "North");
            (Vector.South * 5).ShouldBe(new Vector { Y = -5 }, "South");
            (Vector.East * 0).ShouldBe(default, "Null vector");
        }
    }
}
