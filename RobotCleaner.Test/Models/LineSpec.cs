using RobotCleaner.Enums;
using RobotCleaner.Models;
using Shouldly;
using Xunit;

namespace RobotCleaner.Test.Models
{
    public class LineSpec
    {
        private Point P(int x, int y) => new Point(x, y);

        [Fact]
        public void LineOrientation()
        {
            new Line(P(5, 5), P(5, 5)).Orientation
                .ShouldBeOneOf(
                    new[] { Orientation.Vertical, Orientation.Horizontal },
                    "Expect either vertical or horizontal for points");

            new Line(P(-4, 5), P(6, 5)).Orientation
                .ShouldBe(Orientation.Horizontal, "Expect horizontal orientation when Y values are equal");

            new Line(P(100, 34), P(100, 37)).Orientation
                .ShouldBe(Orientation.Vertical, "Expect vertical orientation when X values are equal");
        }

        [Fact]
        public void Length()
        {
            new Line(P(5, 5), P(5, 5)).Length
                .ShouldBe(1, "Point's length should be 1");

            new Line(P(-4, 5), P(6, 5)).Length
                .ShouldBe(11, "Horizontal line, east");

            new Line(P(6, 5), P(-4, 5)).Length
                .ShouldBe(11, "Horizontal line, west");

            new Line(P(100, 34), P(100, 37)).Length
                .ShouldBe(4, "Vertical line, north");

            new Line(P(100, 37), P(100, 34)).Length
                .ShouldBe(4, "Vertical line, south");
        }
    }
}
