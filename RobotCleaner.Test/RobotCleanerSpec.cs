using RobotCleaner.Models;
using Shouldly;
using System.Linq;
using Xunit;

namespace RobotCleaner.Test
{
    public class RobotCleanerSpec
    {
        [Fact]
        public void RunInstructions_example()
        {
            var robotCleaner = new RobotCleaner();

            var result = robotCleaner.RunInstructions(Instruction.Parse(new[]
            {
                "E 2",
                "N 1",
            }));

            result.ShouldBe(4);
        }

        [Fact]
        public void RunInstructions_single_move()
        {
            var robotCleaner = new RobotCleaner();

            var result = robotCleaner.RunInstructions(Instruction.Parse(new[]
            {
                "W 12",
            }));

            result.ShouldBe(13);
        }

        [Fact]
        public void RunInstructions_back_and_forth()
        {
            var robotCleaner = new RobotCleaner();

            var result = robotCleaner.RunInstructions(Instruction.Parse(new[]
            {
                "E 2",
                "W 6",
            }));

            result.ShouldBe(7, "East <-> west");

            result = robotCleaner.RunInstructions(Instruction.Parse(new[]
            {
                "S 55",
                "N 10",
            }));

            result.ShouldBe(56, "South <-> north");
        }

        [Fact]
        public void RunInstructions_back_and_forth_intersection()
        {
            var robotCleaner = new RobotCleaner();

            var result = robotCleaner.RunInstructions(Instruction.Parse(new[]
            {
                "E 2",
                "W 6",
                "S 10",
                "E 5",
                "N 10", // Intersection
            }));

            result.ShouldBe(
                1
                + 6
                + 10
                + 5
                + 10 - 1);
        }

        [Fact]
        public void RunInstructions_continue()
        {
            var robotCleaner = new RobotCleaner();

            var result = robotCleaner.RunInstructions(Instruction.Parse(new[]
            {
                "N 10",
                "N 10",
                "E 5",
                "S 10",
                "W 20", // Intersection
            }));

            result.ShouldBe(
                1
                + 10
                + 10
                + 5
                + 10
                + 20 - 1);
        }

        [Fact]
        public void RunInstructions_square()
        {
            var robotCleaner = new RobotCleaner();

            var result = robotCleaner.RunInstructions(Instruction.Parse(new[]
            {
                "N 5",
                "N 5",
                "E 10",
                "S 10",
                "W 10", // Intersection
            }));

            result.ShouldBe(4 * 10);
        }

        [Fact]
        public void RunInstructions_performance_snake()
        {
            var robotCleaner = new RobotCleaner();

            var visitedLocationsPerIteration = 2 * (100_000 + 1);
            var iterationCount = 2500;

            // Create a "snake".
            var result = robotCleaner.RunInstructions(
                Enumerable.Range(0, iterationCount)
                    .SelectMany(_ => Instruction.Parse(new[]
                    {
                        "W 100000",
                        "S 1",
                        "E 100000",
                        "S 1",
                    })));

            result.ShouldBe(1 + iterationCount * visitedLocationsPerIteration);
        }

        [Fact]
        public void RunInstructions_performance_square_spirals()
        {
            var robotCleaner = new RobotCleaner();

            var visitedLocationsPerSquare =
                100_000 + 100_000 + 99_999 + 99_999;

            var squareCount = 2500;

            // Create square "spirals".
            var result = robotCleaner.RunInstructions(
                Enumerable.Range(0, squareCount)
                    .SelectMany(_ => Instruction.Parse(new[]
                    {
                        "E 100000",
                        "N 100000",
                        "W 99999", // Intersection(s)
                        "S 99999", // Intersection(s)
                    })));

            var totalVisitedLocations = squareCount * visitedLocationsPerSquare;
            var intersections = 2 * (squareCount - 1) * (squareCount / 2);

            result.ShouldBe(1 + totalVisitedLocations - intersections);
        }
    }
}
