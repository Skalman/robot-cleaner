using RobotCleaner.Helpers;
using Shouldly;
using System.Linq;
using Xunit;

namespace RobotCleaner.Test.Helpers
{
    public class SegmentHelperSpec
    {
        [Fact]
        public void GetNonOverlappingSegments_sort_non_overlapping()
        {
            var result = SegmentHelper.GetNonOverlappingSegments(new[]
            {
                (11, 12),
                (22, 21),
                (2, 1),
                (31, 32),
            });

            result.ShouldBe(new[]
            {
                (1, 2),
                (11, 12),
                (21, 22),
                (31, 32),
            });
        }

        [Fact]
        public void GetNonOverlappingSegments_combine_overlapping_same_start()
        {
            var result = SegmentHelper.GetNonOverlappingSegments(new[]
            {
                (11, 12),
                (11, 22),
            });

            result.ShouldBe(new[] { (11, 22) });

            result = SegmentHelper.GetNonOverlappingSegments(new[]
             {
                (11, 22),
                (11, 12),
            });

            result.ShouldBe(new[] { (11, 22) });
        }

        [Fact]
        public void GetNonOverlappingSegments_overlapping_non_overlapping_and_adjacent()
        {
            var result = SegmentHelper.GetNonOverlappingSegments(new[]
            {
                (11, 12),
                (21, 22),
                (1, 2),
                (31, 32),
                (41, 42),
                (51, 52),
                (61, 62),

                // Overlaps
                (-4, 1),
                (22, 25),
                (30, 33),
                (51, 51), // Point

                // Adjacent
                (43, 45),
            });

            result.ShouldBe(new[]
            {
                (-4, 2),
                (11, 12),
                (21, 25),
                (30, 33),
                (41, 42),
                (43, 45),
                (51, 52),
                (61, 62),
            });
        }

        [Fact]
        public void GetNonOverlappingSegments_nothing()
        {
            var result = SegmentHelper.GetNonOverlappingSegments(Enumerable.Empty<(int, int)>());
            result.ShouldBe(Enumerable.Empty<(int, int)>());
        }

        [Fact]
        public void GetNonOverlappingSegments_single_point()
        {
            var result = SegmentHelper.GetNonOverlappingSegments(new[]
            {
                (6, 6),
            });
            result.ShouldBe(new[] { (6, 6) });
        }

        [Fact]
        public void GetNonOverlappingSegments_single_segment()
        {
            var result = SegmentHelper.GetNonOverlappingSegments(new[]
            {
                (2, 4),
            });
            result.ShouldBe(new[] { (2, 4) }); 
            
            result = SegmentHelper.GetNonOverlappingSegments(new[]
            {
                (4, 2),
            });
            result.ShouldBe(new[] { (2, 4) });
        }
    }
}
