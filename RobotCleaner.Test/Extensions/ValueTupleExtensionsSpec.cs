using RobotCleaner.Extensions;
using Shouldly;
using Xunit;

namespace RobotCleaner.Test.Extensions
{
    public class ValueTupleExtensionsSpec
    {
        [Fact]
        public void MinMax()
        {
            (1, 2).MinMax().ShouldBe((1, 2), "Presorted");
            (2, 1).MinMax().ShouldBe((1, 2), "Resorted");
            (4, 4).MinMax().ShouldBe((4, 4), "Point");
        }
    }
}
