import * as fs from "fs";
import { Instruction, RobotCleaner } from "./RobotCleaner";

setTimeout(start, 0);

async function start() {
  const rawData = fs.readFileSync(0, "utf-8");

  const [
    instructionCount,
    ,
    // The initial position isn't needed.
    ...instructionStrings
  ] = rawData.split(/\r?\n/g).filter((x) => x);

  const instructions = instructionStrings
    .slice(0, +instructionCount)
    .map(Instruction.parse);

  const robot = new RobotCleaner();
  const result = robot.runInstructions(instructions);
  console.log("=>", result);
}
