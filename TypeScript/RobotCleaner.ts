export class RobotCleaner {
  /**
   * Run the given instructions and return the number of unique
   * locations that were cleaned.
   */
  runInstructions(instructions: Instruction[]) {
    // Set the initial position to (0, 0). We only need to keep track
    // of relative positions.
    let curPos = new Point(0, 0);
    const lines: Line[] = [];
    for (const instruction of instructions) {
      const oldPos = curPos;
      curPos = instruction.getNewPosition(oldPos);
      lines.push(new Line(oldPos, curPos));
    }

    return this.getUniqueNumberOfPoints(lines);
  }

  private getUniqueNumberOfPoints(lines: Line[]) {
    const verticalLines = this.getUniqueVerticalLines(
      lines.filter((x) => x.orientation === "vertical")
    );
    const horizontalLines = this.getUniqueHorizontalLines(
      lines.filter((x) => x.orientation === "horizontal")
    );

    const verticalSum = this.totalLength(verticalLines);
    const horizontalSum = this.totalLength(horizontalLines);

    // These are the hot loops that should be optimized.
    // Performance: O(N^2)
    let intersectionCount = 0;
    for (const v of verticalLines) {
      const vy1 = v.p1.y;
      const vy2 = v.p2.y;
      const vx = v.p1.x;
      for (const h of horizontalLines) {
        const hx1 = h.p1.x;
        const hx2 = h.p2.x;
        const hy = h.p1.y;
        if (
          // Vertical line's X is within the horizontal line.
          hx1 <= vx &&
          vx <= hx2 &&
          // Horizontal line's Y is within the vertical line.
          vy1 <= hy &&
          hy <= vy2
        ) {
          intersectionCount++;
        }
      }
    }

    const result = verticalSum + horizontalSum - intersectionCount;

    return result;
  }

  getUniqueVerticalLines(lines: Line[]) {
    return groupBy(lines, (line) => line.p1.x)
      .map(({ key, items }) => {
        const segments: [number, number][] = getNonOverlappingSegments(
          items.map((line) => [line.p1.y, line.p2.y])
        );
        return segments.map(
          (segment) =>
            new Line(new Point(key, segment[0]), new Point(key, segment[1]))
        );
      })
      .flat();
  }

  getUniqueHorizontalLines(lines: Line[]) {
    return lines;
  }

  totalLength(lines: Line[]) {
    return lines.reduce((sum, line) => sum + line.length, 0);
  }
}

function groupBy<TKey, TItem>(
  items: TItem[],
  keySelector: (item: TItem) => TKey
) {
  const result: { key: TKey; items: TItem[] }[] = [];
  const groups = new Map<TKey, TItem[]>();
  for (const item of items) {
    const key = keySelector(item);
    const group = groups.get(key);
    if (group) {
      group.push(item);
    } else {
      const items = [item];
      result.push({ key, items });
      groups.set(key, items);
    }
  }

  return result;
}

/**
 * Given a number of (one-dimensional) line segments, combine them to
 * avoid any overlaps.
 */
function getNonOverlappingSegments(segments: [start: number, end: number][]) {
  const orderedSegments = segments
    .map(([start, end]): [min: number, max: number] =>
      start < end ? [start, end] : [end, start]
    )
    .sort((a, b) => a[0] - b[0]);

  const result: [min: number, max: number][] = [];

  for (const [min, max] of orderedSegments) {
    // This inner while loop should be O(1) on average.
    let newMin = min;
    let newMax = max;
    while (result.length != 0) {
      const [lastMin, lastMax] = result[result.length - 1];

      // Can we combine the current segment with the last one?
      if (lastMax < min) break;

      newMin = Math.min(newMin, lastMin);
      newMax = Math.max(newMax, lastMax);
      result.pop();
    }

    result.push([newMin, newMax]);
  }

  return result;
}

class Line {
  constructor(readonly p1: Point, readonly p2: Point) {}

  get orientation() {
    return this.p1.x === this.p2.x ? "vertical" : "horizontal";
  }

  get length() {
    return (
      1 +
      Math.abs(
        this.p1.x === this.p2.x ? this.p1.y - this.p2.y : this.p1.x - this.p2.x
      )
    );
  }
}

class Point {
  constructor(readonly x: number, readonly y: number) {}

  add(v: Vector) {
    return new Point(this.x + v.x, this.y + v.y);
  }
}

class Vector {
  static readonly east = new Vector({ x: 1 });
  static readonly west = new Vector({ x: -1 });
  static readonly north = new Vector({ y: 1 });
  static readonly south = new Vector({ y: -1 });

  readonly x: number;
  readonly y: number;

  constructor({ x, y }: { x?: number; y?: number } = {}) {
    this.x = x ?? 0;
    this.y = y ?? 0;
  }

  mul(factor: number) {
    return new Vector({
      x: this.x * factor,
      y: this.y * factor,
    });
  }
}

export class Instruction {
  private static readonly directions = {
    E: Vector.east,
    W: Vector.west,
    N: Vector.north,
    S: Vector.south,
  };

  constructor(
    readonly direction: keyof typeof Instruction.directions,
    readonly length: number
  ) {}

  static parse(str: string) {
    const [direction, length] = str.split(" ");
    return new Instruction(
      direction as keyof typeof Instruction.directions,
      +length
    );
  }

  getNewPosition(originalPosition: Point) {
    const vector = Instruction.directions[this.direction].mul(this.length);
    return originalPosition.add(vector);
  }
}
