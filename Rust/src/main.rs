use itertools::Itertools;
use run_instructions::run_instructions;
use std::{
    io::{self, BufRead},
    ops,
};
mod run_instructions;
mod segment;
#[cfg(test)]
mod test;

fn main() {
    let stdin = io::stdin();

    let instructions = stdin
        .lock()
        .lines()
        .skip(2)
        .map(|line| Instruction::parse(line.unwrap()).unwrap())
        .collect_vec();

    match run_instructions(instructions) {
        Ok(value) => println!("=> Cleaned: {}", value),
        Err(err) => panic!("FAILED!\n{}", err),
    };
}

#[derive(Debug, Clone, Copy)]
pub struct Instruction {
    vector: Vector,
}

impl Instruction {
    fn new(direction: &str, length: i64) -> Option<Instruction> {
        let vector = match direction {
            "E" => Vector::EAST,
            "W" => Vector::WEST,
            "N" => Vector::NORTH,
            "S" => Vector::SOUTH,
            _ => return None,
        };

        Some(Instruction {
            vector: vector * length,
        })
    }

    fn parse(str: String) -> Option<Instruction> {
        let (direction, length) = str.split(" ").collect_tuple()?;
        let length = length.parse::<i64>().ok()?;
        Instruction::new(direction, length)
    }

    fn get_new_position(self, original_position: Point) -> Point {
        original_position + self.vector
    }
}

#[derive(Debug, Default, Clone, Copy)]
struct Vector {
    x: i64,
    y: i64,
}

impl Vector {
    // Use a coordinate system where the Y axis increases toward the north.
    pub const EAST: Vector = Vector { x: 1, y: 0 };
    pub const WEST: Vector = Vector { x: -1, y: 0 };
    pub const NORTH: Vector = Vector { x: 0, y: 1 };
    pub const SOUTH: Vector = Vector { x: 0, y: -1 };
}

impl ops::Mul<i64> for Vector {
    type Output = Vector;

    fn mul(self, rhs: i64) -> Self::Output {
        Vector {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

#[derive(Debug, Default, Clone, Copy)]
struct Point {
    x: i64,
    y: i64,
}

impl ops::Add<Vector> for Point {
    type Output = Point;

    fn add(self, rhs: Vector) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

#[derive(Debug)]
enum Line {
    Vertical(VerticalLine),
    Horizontal(HorizontalLine),
}

#[derive(Debug, Clone, Copy)]
struct VerticalLine {
    x: i64,
    y1: i64,
    y2: i64,
}

#[derive(Debug, Clone, Copy)]
struct HorizontalLine {
    y: i64,
    x1: i64,
    x2: i64,
}

impl VerticalLine {
    fn len(self) -> i64 {
        1 + (self.y1 - self.y2).abs()
    }
}

impl HorizontalLine {
    fn len(self) -> i64 {
        1 + (self.x1 - self.x2).abs()
    }
}

impl Into<Line> for VerticalLine {
    fn into(self) -> Line {
        Line::Vertical(self)
    }
}

impl Into<Line> for HorizontalLine {
    fn into(self) -> Line {
        Line::Horizontal(self)
    }
}

impl Line {
    fn new(p1: Point, p2: Point) -> Result<Line, String> {
        if p1.y == p2.y {
            Ok(HorizontalLine {
                y: p1.y,
                x1: p1.x,
                x2: p2.x,
            }
            .into())
        } else if p1.x == p2.x {
            Ok(VerticalLine {
                x: p1.x,
                y1: p1.y,
                y2: p2.y,
            }
            .into())
        } else {
            Err("The line must be either horizontal or vertical".to_string())
        }
    }
}
