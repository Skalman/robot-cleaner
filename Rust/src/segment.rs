#[derive(Debug, PartialEq, Eq)]
pub struct Segment(pub i64, pub i64);

impl Segment {
    pub fn min_max(self) -> Segment {
        if self.0 < self.1 {
            self
        } else {
            Segment(self.1, self.0)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test::ShouldEq;

    mod min_max {
        use super::*;

        #[test]
        fn test_presorted() {
            Segment(1, 2).min_max().should_be(&Segment(1, 2));
        }

        #[test]
        fn test_resorted() {
            Segment(2, 1).min_max().should_be(&Segment(1, 2));
        }

        #[test]
        fn test_point() {
            Segment(4, 4).min_max().should_be(&Segment(4, 4));
        }
    }
}
