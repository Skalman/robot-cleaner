use std::fmt::Debug;

pub trait ShouldEq {
    fn should_be(&self, expected: &Self);
    fn should_be_msg(&self, expected: &Self, msg: &str);
}

impl<T> ShouldEq for T
where
    T: PartialEq + Debug,
{
    fn should_be(&self, expected: &Self) {
        if self != expected {
            eprintln!("Got {:?}, but expected {:?}", self, expected);
            panic!();
        }
    }

    fn should_be_msg(&self, expected: &Self, msg: &str) {
        if self != expected {
            eprintln!("Got {:?}, but expected {:?}", self, expected);
            eprintln!("Message: {}", msg);
            panic!();
        }
    }
}
