use crate::{segment::Segment, HorizontalLine, Instruction, Line, Point, VerticalLine};
use itertools::Itertools;
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use std::cmp;

/// Run the given instructions and return the number of unique
/// locations that were cleaned.
pub fn run_instructions(instructions: Vec<Instruction>) -> Result<i64, String> {
    // Set the initial position to (0, 0). We only need to keep track
    // of relative positions.
    let mut cur_pos: Point = Default::default();
    let mut lines = Vec::new();

    for instruction in instructions {
        let old_pos = cur_pos;
        cur_pos = instruction.get_new_position(old_pos);
        lines.push(Line::new(old_pos, cur_pos)?);
    }

    Ok(get_unique_number_of_points(lines))
}

fn get_unique_number_of_points(lines: Vec<Line>) -> i64 {
    let vertical_lines = lines
        .iter()
        .filter_map(|line| match line {
            Line::Vertical(line) => Some(*line),
            _ => None,
        })
        .collect();

    let horizontal_lines = lines
        .iter()
        .filter_map(|line| match line {
            Line::Horizontal(line) => Some(*line),
            _ => None,
        })
        .collect();

    let vertical_lines = get_unique_vertical_lines(vertical_lines);
    let horizontal_lines = get_unique_horizontal_lines(horizontal_lines);

    let vertical_sum: i64 = vertical_lines.iter().map(|x| x.len()).sum();
    let horizontal_sum: i64 = horizontal_lines.iter().map(|x| x.len()).sum();

    // These are the hot loops that should be optimized.
    // Performance: O(N^2)

    let intersection_count: i64 = vertical_lines
        .par_iter()
        .map(|v| {
            let mut intersection_count = 0;
            for h in &horizontal_lines {
                if
                // Vertical line's X is within the horizontal line.
                h.x1 <= v.x && v.x <= h.x2

                // Horizontal line's Y is within the vertical line.
                && v.y1 <= h.y && h.y <= v.y2
                {
                    intersection_count += 1;
                }
            }
            intersection_count
        })
        .sum();

    let result = vertical_sum + horizontal_sum - intersection_count;

    result
}

fn get_unique_vertical_lines(lines: Vec<VerticalLine>) -> Vec<VerticalLine> {
    lines
        .iter()
        .into_group_map_by(|line| line.x)
        .iter()
        .flat_map(|(x, lines)| {
            let segments = lines.iter().map(|line| Segment(line.y1, line.y2));
            let segments = get_non_overlapping_segments(segments);
            let lines: Vec<VerticalLine> = segments
                .iter()
                .map(|segment| VerticalLine {
                    x: *x,
                    y1: segment.0,
                    y2: segment.1,
                })
                .collect();
            lines
        })
        .collect()
}

fn get_unique_horizontal_lines(lines: Vec<HorizontalLine>) -> Vec<HorizontalLine> {
    lines
        .iter()
        .into_group_map_by(|line| line.y)
        .iter()
        .flat_map(|(y, lines)| {
            let segments = lines.iter().map(|line| Segment(line.x1, line.x2));
            let segments = get_non_overlapping_segments(segments);
            let lines: Vec<HorizontalLine> = segments
                .iter()
                .map(|segment| HorizontalLine {
                    y: *y,
                    x1: segment.0,
                    x2: segment.1,
                })
                .collect();
            lines
        })
        .collect()
}

/// Given a number of (one-dimensional) line segments, combine them to
/// avoid any overlaps.
fn get_non_overlapping_segments(segments: impl Iterator<Item = Segment>) -> Vec<Segment> {
    let segments = segments
        .map(|segment| segment.min_max())
        .sorted_by_key(|segment| segment.0);

    let mut result = Vec::new();

    for Segment(min, max) in segments {
        // This inner while loop should be O(1) on average.
        let mut new_min = min;
        let mut new_max = max;

        while let Some(Segment(last_min, last_max)) = result.last() {
            // Can we combine the current segment with the last one?
            if *last_max < min {
                break;
            }

            new_min = cmp::min(new_min, *last_min);
            new_max = cmp::max(new_max, *last_max);
            result.pop();
        }

        result.push(Segment(new_min, new_max));
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test::ShouldEq;

    fn run(instructions: Vec<&str>) -> i64 {
        let instructions = instructions
            .iter()
            .map(|i| Instruction::parse(i.to_string()).unwrap())
            .collect_vec();

        run_instructions(instructions).unwrap()
    }

    #[test]
    fn test_example() {
        run(vec!["E 2", "N 1"]).should_be(&4);
    }

    #[test]
    fn test_single_move() {
        run(vec!["W 12"]).should_be(&13);
    }

    #[test]
    fn test_back_and_forth() {
        run(vec!["E 2", "W 6"]).should_be(&7);
        run(vec!["S 55", "N 10"]).should_be(&56);
    }

    #[test]
    fn test_continue() {
        run(vec!["N 10", "N 10"]).should_be(&21);
    }

    #[test]
    fn test_square() {
        run(vec!["N 10", "E 10", "S 10", "W 10"]).should_be(&(4 * 10));
    }

    #[test]
    fn test_performance_snake() {
        let visited_locations_per_iteration = 2 * (100_000 + 1);
        let iteration_count = 2500;

        // Create a "snake".
        let instructions = (0..iteration_count)
            .flat_map(|_| &["W 100000", "S 1", "E 100000", "S 1"])
            .map(|i| *i)
            .collect_vec();

        run(instructions).should_be(&(1 + iteration_count * visited_locations_per_iteration));
    }

    #[test]
    fn test_performance_square_spirals() {
        let visited_locations_per_square = 100_000 + 100_000 + 99_999 + 99_999;

        let square_count = 2500;

        // Create square "spirals".
        let instructions = (0..square_count)
            .flat_map(|_| {
                &[
                    "E 100000", //
                    "N 100000", //
                    "W 99999",  // Intersection(s)
                    "S 99999",  // Intersection(s)
                ]
            })
            .map(|i| *i)
            .collect_vec();

        let total_visited_locations = square_count * visited_locations_per_square;
        let intersections = 2 * (square_count - 1) * (square_count / 2);

        run(instructions).should_be(&(1 + total_visited_locations - intersections));
    }
}
